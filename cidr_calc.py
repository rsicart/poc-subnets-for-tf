#!/usr/bin/env python3

import os
import ipaddress
import logging

def get_subnets(vpc_cidr=None, zones=None, number_of_public_subnets=None):
    """Calculates public and private subnets.

    Keyword arguments:
    vpc_cidr -- cidr of network to split
    zones -- list of zones
    number_of_public_subnets -- number of public and private zones to calculate

    Returns tuple of lists
    """
    # how many subnets of each type (public/private)?
    if number_of_public_subnets is None:
        # set same number of public and private subnets
        number_of_public_subnets = number_of_private_subnets = len(available_zones)
    logging.debug('number of public subnets: {}, number of private subnets: {}'.format(number_of_public_subnets, number_of_private_subnets))

    vpc_cidr = ipaddress.IPv4Network(os.environ.get('TF_VAR_cidr_block'))
    if vpc_cidr is None:
        raise ValueError('Error: cidr_block is mandatory')

    vpc_subnets = list(vpc_cidr.subnets(prefixlen_diff=number_of_public_subnets))
    logging.debug(vpc_subnets)
    if len(vpc_subnets) < (len(available_zones) * 2):
        raise ValueError(
                'Error: you need {} subnets for a {} zone setup with public/private subnets'.format(
                    len(available_zones) * 2,
                    len(available_zones)
                )
        )

    public_subnets = vpc_subnets[0:number_of_public_subnets]
    logging.debug('Public subnets are: {}'.format(public_subnets))

    private_subnets = vpc_subnets[number_of_public_subnets:number_of_public_subnets+number_of_private_subnets]
    logging.debug('Private subnets are: {}'.format(private_subnets))
    return (public_subnets, private_subnets)


def print_shell_script(subnets=None):
    """Prints a shell script.

    Keyword arguments:
    subnets -- tuple of lists, containing one list of public and 1 list of private subnets
    """
    if subnets is None:
        raise ValueError('Error: subnets is mandatory')

    public_kv_strings = ['{}={}'.format(elem[0], elem[1]) for elem in list(zip(zones, subnets[0]))]
    private_kv_strings = ['{}={}'.format(elem[0], elem[1]) for elem in list(zip(zones, subnets[1]))]

    print("""#!/bin/bash
export TF_VAR_cidr_public={{{}}}
export TF_VAR_cidr_private={{{}}}
""".format(','.join(public_kv_strings), ','.join(private_kv_strings)))


if __name__ == '__main__':

    # configure logging
    debug = os.environ.get('DEBUG')
    if debug is not None:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    region = os.environ.get('TF_VAR_region')
    if region is None:
        raise ValueError('Error: region is mandatory')

    available_zones_csv = os.environ.get('AVAILABLE_ZONES_CSV')
    try:
        available_zones = available_zones_csv.split(',')
    except:
        available_zones = ['a', 'b']

    zones = ['{}{}'.format(region, zone) for zone in available_zones]
    logging.debug(zones)

    vpc_cidr = ipaddress.IPv4Network(os.environ.get('TF_VAR_cidr_block'))
    subnets = get_subnets(vpc_cidr=vpc_cidr, zones=zones)

    print_shell_script(subnets=subnets)
